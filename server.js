const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.json());

class Doctor {
  constructor(name, consultationTime) {
    this.name = name;
    this.consultationTime = consultationTime; // Average consultation time for the doctor
  }
}

/**
 * Function to calculate the estimated waiting time for a patient.
 * @param {Doctor[]} doctors - An array of Doctor objects.
 * @param {number} patientPosition - Position of the patient in the queue.
 * @returns {number} - Estimated waiting time for the patient in minutes.
 */
function calculateWaitingTime(doctors, patientPosition) {
  // Constants
  const AVERAGE_CONSULTATION_TIME = doctors.reduce((sum, doctor) => sum + doctor.consultationTime, 0) / doctors.length;
  const PATIENTS_IN_QUEUE = patientPosition - 1;

  // Calculate estimated waiting time
  const waitingTime = AVERAGE_CONSULTATION_TIME * PATIENTS_IN_QUEUE;

  return waitingTime;
}

/**
 * API endpoint to calculate the estimated waiting time for a patient.
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 */
app.post('/calculate-waiting-time', (req, res) => {
  const doctors = req.body.doctors; // An array of Doctor objects
  const patientPosition = req.body.patientPosition; // Position of the patient in the queue

  const estimatedWaitingTime = calculateWaitingTime(doctors, patientPosition);

  res.json({ estimatedWaitingTime });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});